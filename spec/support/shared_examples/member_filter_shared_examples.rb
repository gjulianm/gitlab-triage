RSpec.shared_examples 'a member filter', :type do
  include_context 'with network context'

  let(:username) { 'joe_bloggs' }
  let(:resource) do
    {
      type => {
        username: username
      }
    }
  end
  let(:group_id) { 9970 }
  let(:condition) do
    {
      source: 'group',
      condition: 'member_of',
      source_id: group_id
    }
  end

  let(:usernames) { [username, 'b'] }
  let(:members) do
    [
      { username: usernames[0] },
      { username: usernames[1] }
    ]
  end

  before do
    allow(subject).to receive(:members).and_return(members)
  end

  subject { described_class.new(resource, condition, network) }

  it_behaves_like 'a filter'

  describe '#resource_value' do
    it 'has the correct value for updated_at attribute' do
      expect(subject.resource_value).to eq(username)
    end
  end

  describe '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(usernames)
    end
  end

  describe '#calculate' do
    context 'when condition matches' do
      it 'returns true' do
        expect(subject.calculate).to eq(true)
      end

      context 'when resource member is nil' do
        before do
          resource[type] = nil
        end

        it 'calculate false' do
          expect(subject.calculate).to eq(false)
        end
      end
    end

    context 'when condition does not match' do
      let(:condition) do
        {
          source: 'group',
          condition: 'not_member_of',
          source_id: group_id
        }
      end

      it 'returns false' do
        expect(subject.calculate).to eq(false)
      end

      context 'when resource member is nil' do
        before do
          resource[type] = nil
        end

        it 'returns false' do
          expect(subject.calculate).to eq(false)
        end
      end
    end
  end

  describe '#member_url' do
    it 'generates the correct url' do
      expect(subject.member_url).to eq("http://test.com/api/v4/groups/#{group_id}/members?per_page=100")
    end

    context 'when source is a project' do
      let(:project_path) { 'gitlab-org/gitlab-ce' }
      let(:condition) do
        {
          source: 'project',
          condition: 'member_of',
          source_id: project_path
        }
      end

      it 'generate the correct url' do
        expect(subject.member_url).to eq("http://test.com/api/v4/projects/gitlab-org%2Fgitlab-ce/members?per_page=100")
      end
    end
  end
end
